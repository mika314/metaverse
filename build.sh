#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo Script directory ${DIR}

# exctract the top direcotry name
PrjName=$(basename ${DIR})

echo Top directory ${PrjName}

UNREALROOTPATH=/mnt/Texas/prj/ue5.4

cd ${UNREALROOTPATH}
source Engine/Build/BatchFiles/Linux/SetupEnvironment.sh -dotnet Engine/Build/BatchFiles/Linux

COUNTER_FILE="${DIR}/.run_counter"
SUFFIX_FILE="${DIR}/.module_suffix"

if [ ! -f "${COUNTER_FILE}" ]; then
    echo "0" > "${COUNTER_FILE}"
    COUNTER=$(cat "${COUNTER_FILE}")
else
    COUNTER=$(cat "${COUNTER_FILE}")
    COUNTER=$((COUNTER+1))
    echo $COUNTER > "${COUNTER_FILE}"
fi
if [ ! -f "${SUFFIX_FILE}" ]; then
    echo "1" > "${SUFFIX_FILE}"
    MODULE_SUFFIX=1
else
    MODULE_SUFFIX=$(cat "${SUFFIX_FILE}")
    MODULE_SUFFIX=$((MODULE_SUFFIX+1))
    echo $MODULE_SUFFIX > "${SUFFIX_FILE}"
fi


if [ "$COUNTER" -ge 10 ]; then
    dotnet Engine/Binaries/DotNET/UnrealBuildTool/UnrealBuildTool.dll \
           ${PrjName}Editor \
           Linux \
           Development \
           -Project="${DIR}/${PrjName}.uproject" \
           "${DIR}/${PrjName}.uproject"  \
           -IgnoreJunk \
           -progress

    echo "0" > "${COUNTER_FILE}"
fi

dotnet Engine/Binaries/DotNET/UnrealBuildTool/UnrealBuildTool.dll \
       -ModuleWithSuffix=${PrjName},${MODULE_SUFFIX} \
       ${PrjName}Editor \
       Linux \
       Development \
       -Project="${DIR}/${PrjName}.uproject" \
       "${DIR}/${PrjName}.uproject"  \
       -IgnoreJunk \
       -progress
