# Metaverse

Hello, this is a collaborative project in Unreal Engine 5. Unreal
Engine 5 has an open map and one file per actor system which is
supposed to help in collaborative projects. So let's test the limits
of Unreal Engine 5.

Make a PR with anything your imagination can come up with. Keep the
content original and legal, also let's keep it appropriate for all
audiences. I am going to approve and merge everything. One limiting
factor, let's try to keep the repository small. I want this project to
be accessible for everyone with slow Internet or limited hard disk
size.

By adding your original content by default you agreed to release it
with the MIT license. If you want a different license, please
explicitly specify it.

# Installation

You need git with LFS support. You can find instructions on the
 Internet, e.g. from here: https://git-lfs.github.com/

Get the Unreal Engine from the Epic Launcher. You need Visual Studio,
because the project is a C++ project. You can use the community
version of Visual Studio from here:
https://visualstudio.microsoft.com/downloads/

Clone the project from git-bash:

```
git clone --recurse-submodules git@gitlab.com:mika314/metaverse.git
```

Open the file `Metaverse.uproject` and let the engine recompile the
code.
