// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MetaverseGameMode.generated.h"

UCLASS(minimalapi)
class AMetaverseGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMetaverseGameMode();
};



