// MIT License - see license.md file

#pragma once

#include <CoreMinimal.h>
#include <Engine/StaticMeshActor.h>

#include "WelcomeOrb.generated.h"

UCLASS()
class METAVERSE_API AWelcomeOrb final : public AStaticMeshActor
{
  GENERATED_BODY()
public:
  AWelcomeOrb();

  auto Tick(float) -> void final;
  auto BeginPlay() -> void final;

private:
  UFUNCTION()
  void beginOverlap(AActor *self, AActor *other);

  UFUNCTION()
  void playStateChanged(EAudioComponentPlayState state);

  class USphereComponent *sphere;
  class UAudioComponent *welcomeMessage;
};
