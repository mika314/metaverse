// MIT License - see license.md file

#include "WelcomeOrb.h"
#include "MetaverseCharacter.h"
#include <Components/AudioComponent.h>
#include <Components/SphereComponent.h>
#include <QofL/abbr.h>
#include <QofL/check_ret.h>
#include <QofL/class_finder.h>
#include <QofL/obj_finder.h>
#include <Sound/SoundCue.h>
#include <cmath>

AWelcomeOrb::AWelcomeOrb()
  : sphere(CreateDefaultSubobject<USphereComponent>("sphere")),
    welcomeMessage(CreateDefaultSubobject<UAudioComponent>("welcomeMessage"))
{
  auto mesh = GetStaticMeshComponent();
  mesh->SetStaticMesh(OBJ_FINDER(StaticMesh, "FPWeapon/Mesh", "FirstPersonProjectileMesh"));
  sphere->AttachToComponent(mesh, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
  welcomeMessage->AttachToComponent(mesh, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
  welcomeMessage->SetAutoActivate(false);
  welcomeMessage->SetSound(OBJ_FINDER(SoundCue, "Snd", "SND_Welcome_to_the_Metaverse_Cue"));
  sphere->SetSphereRadius(600.f);
  mesh->SetWorldScale3D(vec(.25f, .25f, .25f));
  PrimaryActorTick.bCanEverTick = true;
}

auto AWelcomeOrb::Tick(float dt) -> void
{
  Super::Tick(dt);
  if (welcomeMessage->IsActive())
  {
    const auto s =
      (sinf(GetWorld()->GetTimeSeconds() * 500.f * (2.f * 3.1415f) / 360.f) * 0.2f + 1.f) * .25f;
    RootComponent->SetWorldScale3D(vec(s, s, s));
  }
}

auto AWelcomeOrb::BeginPlay() -> void
{
  Super::BeginPlay();
  OnActorBeginOverlap.AddDynamic(this, &AWelcomeOrb::beginOverlap);
  welcomeMessage->OnAudioPlayStateChanged.AddDynamic(this, &AWelcomeOrb::playStateChanged);
}

auto AWelcomeOrb::beginOverlap(AActor * /*self*/, AActor *other) -> void
{
  auto character = Cast<AMetaverseCharacter>(other);
  if (!character)
    return;
  if (welcomeMessage->IsActive())
    return;
  welcomeMessage->Play(0.f);
}

auto AWelcomeOrb::playStateChanged(EAudioComponentPlayState state) -> void
{
  if (state == EAudioComponentPlayState::Stopped)
    Destroy();
}
